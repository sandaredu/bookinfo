import { createConnection } from "mysql";
var connection = createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'bookstore',
});
connection.connect(function (err) {
    if (err) {
        console.error('Error connecting to MySQL database: ' + err.stack);
        return;
    }
    console.log('Connected to MySQL database as id ' + connection.threadId);
});
var database = [];
//ホームページに戻る
function returnHome() {
    window.location.href = 'http://127.0.0.1:8080/front-end/index.html';
}
document.addEventListener('DOMContentLoaded', function () {
    alert('hello,returnhome test!!');
    var returnHomebutton = document.getElementById('gotohome');
    if (returnHomebutton) {
        alert('hello,returnhome success!!');
        returnHomebutton.addEventListener('click', returnHome);
    }
});
document.addEventListener('DOMContentLoaded', function (event) {
    event.preventDefault();
    alert("hi");
    // Get form data
    var form = event.target;
    var name = form.elements.namedItem('name');
    var price = form.elements.namedItem('price');
    // Create user object
    var book_data = {
        book_id: 1,
        bookname: name.value,
        price: price.value,
        description: "first insert",
        uploaded: new Date(2024 / 3 / 14),
        flag: 1
    };
    // Add user data to the database
    database.push(book_data);
    console.log('Data submitted:', book_data);
    return new Promise(function (resolve, reject) {
        connection.query('INSERT INTO book SET ?', database, function (err, result) {
            if (err) {
                return reject(err);
            }
            resolve(result.insertId);
        });
    });
});
//データベースに挿入するように
// export const insert = (book: Book) => {
//     return new Promise<number>((resolve, reject) => {
//       connection.query('INSERT INTO book SET ?', database, (err, result) => {
//         if (err) {
//           return reject(err);
//         }
//         resolve(result.insertId);
//       });
//     });
//};
