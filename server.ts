// server.ts
import { Request, Response } from 'express';
const express = require('express');
const path = require('path');
const app = express();
const PORT = 3000;

const htmlFilePath = path.join(__dirname, 'front-end/index.html');
const insertfile = path.join(__dirname, 'front-end/insert.html' );

app.use(express.static('back-end', { 'extensions': ['html', 'htm', 'js'] }));
//app.use(express.static(path.join(__dirname, "js")));

// Define a route to serve the HTML file
app.get('/', (req: Request, res: Response) => {
  //res.set('Content-Type', 'application/javascript');
  res.sendFile(htmlFilePath);
  res.sendFile(insertfile);
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});