import * as mysql from 'mysql';

export interface Book {
  book_id: number;
  bookname: string;
  price: string;
  description: string;
  uploaded: Date;
  flag: number;
}

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'bookstore',
});

connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL database: ' + err.stack);
    return;
  }
  console.log('Connected to MySQL database as id ' + connection.threadId);
});
const database: Book[] = [];

//ホームページに戻る
function returnHome() {
    window.location.href = 'http://127.0.0.1:8080/front-end/index.html';
}
document.addEventListener('DOMContentLoaded', function () {
    alert('hello,returnhome test!!');
    var returnHomebutton = document.getElementById('gotohome');
    if (returnHomebutton) {
        alert('hello,returnhome success!!');
        returnHomebutton.addEventListener('click', returnHome);
    }
});

document.addEventListener('DOMContentLoaded', function (event: Event) {
  event.preventDefault();
    alert("hi");
    // Get form data
    const form = event.target as HTMLFormElement;
    const name = form.elements.namedItem('name') as HTMLInputElement;
    const price = form.elements.namedItem('price') as HTMLInputElement;
    console.log("name " + name );
    console.log("price " + price );

    // Create user object
    const book_data: Book = {
        book_id: 1,
        bookname: name.value,
        price: price.value,
        description: "first insert",
        uploaded: new Date(2024/3/14),
        flag: 1
    };

    // Add user data to the database
    database.push(book_data);
    console.log('Data submitted:', book_data);

    return new Promise<number>((resolve, reject) => {
        connection.query('INSERT INTO book SET ?', database, (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve(result.insertId);
        });
      });
});



//データベースに挿入するように
// export const insert = (book: Book) => {
//     return new Promise<number>((resolve, reject) => {
//       connection.query('INSERT INTO book SET ?', database, (err, result) => {
//         if (err) {
//           return reject(err);
//         }
//         resolve(result.insertId);
//       });
//     });
//};