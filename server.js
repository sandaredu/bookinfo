"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var path = require('path');
var app = express();
var PORT = 3000;
var htmlFilePath = path.join(__dirname, 'front-end/index.html');
var insertfile = path.join(__dirname, 'front-end/insert.html');
app.use(express.static('back-end', { 'extensions': ['html', 'htm', 'js'] }));
//app.use(express.static(path.join(__dirname, "js")));
// Define a route to serve the HTML file
app.get('/', function (req, res) {
    //res.set('Content-Type', 'application/javascript');
    res.sendFile(htmlFilePath);
    res.sendFile(insertfile);
});
// Start the server
app.listen(PORT, function () {
    console.log("Server is running on http://localhost:".concat(PORT));
});
