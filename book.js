"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteBook = exports.addBook = exports.getAllBooks = void 0;
var dbconfig_1 = require("./dbconfig");
var getAllBooks = function () {
    return new Promise(function (resolve, reject) {
        dbconfig_1.default.query('SELECT * FROM books', function (err, results) {
            if (err) {
                return reject(err);
            }
            resolve(results);
        });
    });
};
exports.getAllBooks = getAllBooks;
var addBook = function (book) {
    return new Promise(function (resolve, reject) {
        dbconfig_1.default.query('INSERT INTO books SET ?', book, function (err, result) {
            if (err) {
                return reject(err);
            }
            resolve(result.insertId);
        });
    });
};
exports.addBook = addBook;
var deleteBook = function (id) {
    return new Promise(function (resolve, reject) {
        dbconfig_1.default.query('DELETE FROM books WHERE id = ?', id, function (err) {
            if (err) {
                return reject(err);
            }
            resolve();
        });
    });
};
exports.deleteBook = deleteBook;
