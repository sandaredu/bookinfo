import connection from './dbconfig';

export interface Book {
  id?: number;
  title: string;
  author: string;
}

export const getAllBooks = () => {
  return new Promise<Book[]>((resolve, reject) => {
    connection.query('SELECT * FROM books', (err, results) => {
      if (err) {
        return reject(err);
      }
      resolve(results);
    });
  });
};

export const addBook = (book: Book) => {
  return new Promise<number>((resolve, reject) => {
    connection.query('INSERT INTO books SET ?', book, (err, result) => {
      if (err) {
        return reject(err);
      }
      resolve(result.insertId);
    });
  });
};

export const deleteBook = (id: number) => {
  return new Promise<void>((resolve, reject) => {
    connection.query('DELETE FROM books WHERE id = ?', id, (err) => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
};